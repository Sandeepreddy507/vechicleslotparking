package com.VechicleParking.VechicleSlotparking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VechicleparkingApplication {

	public static void main(String[] args) {
		SpringApplication.run(VechicleparkingApplication.class, args);
	}

}
