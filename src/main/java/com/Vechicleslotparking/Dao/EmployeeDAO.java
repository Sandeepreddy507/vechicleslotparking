package com.Vechicleslotparking.Dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
 

import org.springframework.stereotype.Repository;

import com.Vechicleslotparking.model.Employee;
 
@Repository
public class EmployeeDAO {
 
    private static final Map<String, Employee> empMap = new HashMap<String, Employee>();
 
    static {
        initEmps();
    }
 
    private static void initEmps() {
        Employee emp1 = new Employee("E01", "17", "Manager");
        Employee emp2 = new Employee("E02","17",  "Manager");
        Employee emp3 = new Employee("E03", "19", "Director");
        Employee emp4 = new Employee("E03", "10", "Seniorconsultant");
        Employee emp5 = new Employee("E03", "12", "juniorManager");
        Employee emp6 = new Employee("E03", "14", "juniorManager");
        Employee emp7 = new Employee("E03", "8", "techincallead");
        Employee emp8 = new Employee("E03", "4", "consultant");
        Employee emp9 = new Employee("E03", "16", "Manager");
        Employee emp10 = new Employee("E03", "16", "Manager");
        
 
        empMap.put(emp1.getEmpNo(), emp1);
        empMap.put(emp2.getEmpNo(), emp2);
        empMap.put(emp3.getEmpNo(), emp3);
    }
 
    public Employee getEmployee(String empNo) {
        return empMap.get(empNo);
    }
 
    public Employee addEmployee(Employee emp) {
        empMap.put(emp.getEmpNo(), emp);
        return emp;
    }
 
    public Employee updateEmployee(Employee emp) {
        empMap.put(emp.getEmpNo(), emp);
        return emp;
    }
 
    public void deleteEmployee(String empNo) {
        empMap.remove(empNo);
    }
 
    public List<Employee> getAllEmployees() {
        Collection<Employee> c = empMap.values();
        List<Employee> list = new ArrayList<Employee>();
        list.addAll(c);
        return list;
    }
 
}